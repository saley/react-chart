import 'core-js/fn/object/assign';
import React from 'react';
import ReactDOM from 'react-dom';
import Chart from './components/chart/MainComponent';

import json from './data/usd_quotes.js';

let options = {
	i18n: {
		months: {
			'01': ['январь',   'января'],
			'02': ['февраль',  'февраля'],
			'03': ['март',     'марта'],
			'04': ['апрель',   'апреля'],
			'05': ['май',      'мая'],
			'06': ['июнь',     'июня'],
			'07': ['июль',     'июля'],
			'08': ['август',   'августа'],
			'09': ['сентябрь', 'сентябрь'],
			'10': ['октябрь',  'октября'],
			'11': ['ноябрь',   'ноября'],
			'12': ['декабрь',  'декабря']
		}
	},
	chart: {
		size: [1000, 280],
		padding: [25, 20, 55, 35],
		axisYTick: 20,
		dateFormat: '%d %m %Y'
	}
};

ReactDOM.render(<Chart data={json} options={options} />, document.getElementById('app'));
