/**
 *  Column component for react line chart
 */
 
'use strict';

import React from 'react';

class ColumnComponent extends React.Component {
	componentWillMount() {
		this.setState({
			isHover: false
		});
	}

	render() {
		let transform = `translate(${this.props.data.x},0)`;

		return (
			<g className="chart__content__item" transform={transform}>
				<rect className="chart__axisX__tick"
						width={this.props.width}
						height={this.props.height}
						onMouseOver={this.onMouseOver.bind(this)}
						onMouseOut={this.onMouseOut.bind(this)}
				></rect>

				{this.renderTooltip()}
			</g>
		);
	}

	/**
	*  Render tooltip with extra data
	*/
	renderTooltip() {
		if (!this.state.isHover) {
			return;
		}

		let y = this.props.data.y;

		return (
			<g className="chart__tooltip">
				<rect rx="5" ry="5" y={y} className="chart__tooltip__bg"/>
				<line className="chart__tooltip__grid" y1={y} y2={this.props.height} />
				<circle className="chart__tooltip__point" cy={y}></circle>
				<text className="chart__tooltip__date" y={y}>{this.getDateFormatted()}</text>
				<text className="chart__tooltip__value" y={y}>$ {this.props.data.value}</text>
				{this.renderDelta()}
			</g>
		);
	}

	/**
	 *  Render delta to previous value markup
	 */
	renderDelta() {
		if (!this.props.data.delta) {
			return;
		}

		let y = this.props.data.y,
			transform = `translate(0,${y})`,
			delta = this.props.data.delta;

		if (this.props.data.delta > 0) {
			return (
				<g transform={transform}>
					<polygon className="chart__tooltip__icon inc" points="5,0 9,7 0,7"></polygon>
					<text className="chart__tooltip__delta inc">{delta}</text>
				</g>
			);
		}

		return (
			<g transform={transform}>
				<polygon className="chart__tooltip__icon dec" points="0,0 9,0 5,7"></polygon>
				<text className="chart__tooltip__delta dec">{delta}</text>
			</g>
		);
	}

	/**
	 *  Format date according config pattern
	 *  @returns {String}
	 */
	getDateFormatted () {
		let [Y, m, d] = this.props.data.date.split('-');

		return this.props.options.chart.dateFormat
			.replace('%Y', Y)
			.replace('%m', this.props.options.i18n.months[m][1])
			.replace('%d', d);
	}

	/**
	 *  Change hover state
	 */
	onMouseOver() {
		this.setState({
			isHover: true
		});
	}

	/**
	 *  Change hover state
	 */
	onMouseOut() {
		this.setState({
			isHover: false
		});
	}
}

ColumnComponent.displayName = 'ChartColumnComponent';

export default ColumnComponent;
