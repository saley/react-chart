/**
 *  Main component for react line chart
 */

'use strict';

import React from 'react';
import Column from './ColumnComponent';

require('styles//chart/Main.less');

class MainComponent extends React.Component {
	render() {
		let axisYTransform = `translate(0, ${this.state.chart.paddingTop})`,
			axisXTransform = `translate(${this.state.chart.paddingLeft},  ${this.state.chart.height - 35})`,
			contentTransform = `translate(${this.state.chart.paddingLeft}, ${this.state.chart.paddingTop})`,
			polylinePoints = this.state.data.map( (item) => `${item.x},${item.y}` ).join(' '),
			axisXData = this.getAxisXData();

		return (
			<svg className="chart" width={this.state.chart.width} height={this.state.chart.height}>
				<g className="chart__axisY" transform={axisYTransform}>
					{this.getAxisYData().map( (tick) => {
						let transform = `translate(0,${tick.translateY})`;

					    return (
							<g className="chart__axisY__tick" key={tick.id} transform={transform}>
								<line className="chart__axisY__line" x1={tick.x1} x2={tick.x2}></line>
								<text className="chart__axisY__text">{tick.label}</text>
							</g>
					    );
					})}
				</g>
				<g className="chart__axisX" transform={axisXTransform}>
					<g className="chart__axisX__months">
						{axisXData.months.map( (month) =>
							<text className="chart__axisX__text" key={month.id} x={month.offset}>{month.label}</text>
						)}
					</g>
					<g className="chart__axisX__years">
						{axisXData.years.map( (year) =>
							<text className="chart__axisX__text" key={year.id} x={year.offset} y="20">{year.label}</text>
						)}
					</g>
				</g>
				<g className="chart__content" transform={contentTransform}>
					<polyline className="chart__content__line" points={polylinePoints}></polyline>

					{this.state.data.map((item) =>
						<Column key={item.date}
								data={item}
								options={this.props.options}
								width={this.state.axisX.tick}
								height={this.state.content.height}
						/>
					)};
				</g>
				<filter id="chart_dropshadow" height="130%">
					<feGaussianBlur in="SourceAlpha" stdDeviation="3"/>
					<feOffset dx="0" dy="2" result="offsetblur"/>
					<feMerge> 
						<feMergeNode/>
						<feMergeNode in="SourceGraphic"/>
					</feMerge>
				</filter>
			</svg>
		);
	}

	/**
	 *  Adapt incoming data and calc chart params
	 */
	componentWillMount() {
		let [chartWidth, chartHeight] = this.props.options.chart.size;
		let [chartPaddingTop, chartPaddingRight, chartPaddingBottom, chartPaddingLeft] = this.props.options.chart.padding;
		
		let contentWidth = chartWidth - chartPaddingRight - chartPaddingLeft,
			contentHeight = chartHeight - chartPaddingTop - chartPaddingBottom;
		
		let data = Object.keys(this.props.data).map( (key) => {
			return {
				date: key,
				value: this.props.data[key].toFixed(2)
			}
		});

		data.sort((a, b) => {
			return Date.parse(a.date) - Date.parse(b.date);
		});

		let max = Math.max(...data.map(item => item.value));
		let min = Math.min(...data.map(item => item.value));

		let axisXTick = contentWidth / (data.length - 1);

		let axisYTick = this.props.options.chart.axisYTick,
			axisYMaxValue = Math.ceil(max / axisYTick) * axisYTick;
		
		data.forEach((item, i, items) => {
			let delta = i ? (item.value - items[i - 1].value).toFixed(2) : null,
				valueRelative = item.value / axisYMaxValue;

			Object.assign(item, {
				delta: delta,
				valueRelative: valueRelative,
				x: i * axisXTick,
				y: contentHeight * (1 - valueRelative)
			});
		});

		this.setState({
			data: data,
			dataMax: max,
			dataMin: min,

			chart: {
				width: chartWidth,
				height: chartHeight,

				paddingTop: chartPaddingTop,
				paddingRight: chartPaddingRight,
				paddingBottom: chartPaddingBottom,
				paddingLeft: chartPaddingLeft
			},
			content: {
				width: contentWidth,
				height: contentHeight
			},
			axisX: {
				tick: axisXTick
			},
			axisY: {
				tick: axisYTick,
				max: axisYMaxValue
			}
		});
	}

	/**
	 *  Prepare axisY ticks data
	 *  @returns {Array}
	 */
	getAxisYData() {
		let axisY = [];

		for (let i = this.state.axisY.max; i >= 0; i -= this.state.axisY.tick) {
			axisY.push(i);
		}

		return axisY.map((tick, i, ticks) => {
			return {
				id: 'axisY' + tick,
				label: tick,
				x1: this.state.chart.paddingLeft,
				x2: this.state.chart.width - this.state.chart.paddingRight,
				translateY: i * this.state.content.height / (ticks.length - 1)
			};
		});
	}

	/**
	 *  Prepare axisX ticks data
	 *  @returns {Object}
	 */
 	getAxisXData() {
		let axisX = {
			months: [],
			years: []
		}, year, month;

		this.state.data.forEach((item, i) => {
			let [Y, m] = item.date.split('-'),
				offset = i * this.state.axisX.tick;

			if (Y !== year) {
				axisX.years.push({
					id: item.date,
					offset: offset,
					label: Y
				});
				year = Y;
			}

			if (m !== month) {
				axisX.months.push({
					id: item.date,
					offset: offset,
					label: this.props.options.i18n.months[m][0]
				});
				month = m;
			}
		});

		return axisX;
	}
}

MainComponent.displayName = 'ChartMainComponent';

export default MainComponent;
